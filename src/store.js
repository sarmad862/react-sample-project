import {createStore, combineReducers, applyMiddleware} from "redux";
import {createLogger} from 'redux-logger';

//Reducers Importing 

import User from './reducers/Userreducer';
import Math  from './reducers/Mathreducer'


export default  createStore(combineReducers({Math,User}),
  {},
  applyMiddleware( createLogger ())
  );
  

