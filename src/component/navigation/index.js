import React from 'react';
import { Button } from 'antd';
import {NavLink,withRouter} from 'react-router-dom'; 
import './Navigation.scss'
class Navigation extends React.Component{
   
    render(){
        return(
            <div>      
                 <div className="nas">
                        <div>
                            <h1>
                           Sample
                            </h1>
                        </div>

                        <div className="navigation"> 
                            <div className="nav-list">
                                <ul>
                                  <li><NavLink to="/">Home</NavLink></li>
                                  <li><NavLink to="/about">About</NavLink></li>
                                  <li><NavLink to="/packages">Packages</NavLink></li>
                                  <li><NavLink to="/contact">Contact</NavLink></li>
                                </ul>
                            </div>
                            <div className="btn">
                             
                                <Button type="link" onClick={()=>this.props.history.push('/login')}>Login</Button>
                                <Button type="link" onClick={()=>this.props.history.push('/signup')}>Signup</Button>
                            </div>
                        </div>
                     
                 </div>
            </div>
        );
    }

}

export default withRouter(Navigation);
