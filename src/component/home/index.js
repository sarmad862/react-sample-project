import React from 'react';

import Home from "./Home";
import {Btncomponent} from "./Btncomponent"


class index extends React.Component{
    constructor(props) {
        super(props);
    
        console.log(this.props.changeUsername)
    }
    render(){
        return(
           
            <div className="container">
                {/* <h1></h1> */}
                <Home name={this.props.name}/>
                <Btncomponent changeUsername={this.props.changeUsername}/>
             
            </div>
        );
    }

}

export default index;