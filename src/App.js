import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import {connect} from 'react-redux';
//css  files included custom and AntD 
import './App.scss';
import 'antd/dist/antd.css';

//include components
import Router from './Router';
import Nav from './component/navigation';
import { Layout } from 'antd';


//action import
import {setName} from './actions/useraction'
const { Header, Footer, Content } = Layout;

class App extends React.Component {
 
  
  render(){
  return (
   <BrowserRouter>
    <Layout>
      <Header>
        <Nav/>
      </Header>
      <Content  >  
       <Router
        username={this.props.user.name}
        changeUsername={()=>this.props.setName("Anna")}>
       </Router>
      </Content>
  <Footer></Footer>
    </Layout>
    </BrowserRouter>
  );
}
}

const mapStateToProps=(state)=>{
  return{
    user:state.userReducer,
    Math:state.mathReducer
}
};

const mapDispatchToProps =(dispatch)=>{
  return{
    setName:(name)=>{
      dispatch(setName(name))
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(App);

