import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {createStore, combineReducers, applyMiddleware} from "redux";
import { createLogger } from 'redux-logger';
import {Provider} from 'react-redux';
import App from './App';

//importing Store

import store from './store';

ReactDOM.render(
  <Provider store={store}>
    <App />
    </Provider>
    ,
  document.getElementById('root')
);
