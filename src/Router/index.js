import React from 'react';
import {Route} from 'react-router-dom';

//include components
import Packages from '../component/packages'
import Home from '../component/home'
import About from '../component/about';
import Contact from '../component/contact';
import Login from'../component/login';
import Signup from '../component/signup'

class Router extends React.Component{
   
    render(){
    return(
        <div>
         
        <Route exact path="/"  >
            <Home 
            name={this.props.username} 
            changeUsername={this.props.changeUsername}
            /></Route>
        <Route path="/about" component={About}/>
        <Route path="/packages" component={Packages}/>
        <Route path="/contact" component={Contact}/>
        <Route path="/login" component={Login}/>
        <Route path="/Signup" component={Signup}/>
        </div>
    );
}}

export default Router;